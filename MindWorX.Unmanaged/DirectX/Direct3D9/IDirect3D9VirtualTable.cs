﻿using System;
using System.Runtime.InteropServices;
using MindWorX.Unmanaged.Invokers;

namespace MindWorX.Unmanaged.DirectX.Direct3D9
{
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public unsafe delegate uint GetAdapterCountPrototype(IDirect3D9VirtualObject* direct3D);

    [StructLayout(LayoutKind.Sequential)]
    // ReSharper disable once InconsistentNaming
    public unsafe struct IDirect3D9VirtualTable
    {
        private static GetAdapterCountPrototype GetAdapterCountFunction;

        #region IUnknown

        public IntPtr QueryInterfacePtr;
        public IntPtr AddRefPtr;
        public IntPtr ReleasePtr;

        #endregion

        #region IDirect3D9

        public IntPtr RegisterSoftwareDevicePtr;
        public IntPtr GetAdapterCountPtr;
        public IntPtr GetAdapterIdentifierPtr;
        public IntPtr GetAdapterModeCountPtr;
        public IntPtr EnumAdapterModesPtr;
        public IntPtr GetAdapterDisplayModePtr;
        public IntPtr CheckDeviceTypePtr;
        public IntPtr CheckDeviceFormatPtr;
        public IntPtr CheckDeviceMultiSampleTypePtr;
        public IntPtr CheckDepthStencilMatchPtr;
        public IntPtr CheckDeviceFormatConversionPtr;
        public IntPtr GetDeviceCapsPtr;
        public IntPtr GetAdapterMonitorPtr;
        public IntPtr CreateDevicePtr;

        public GetAdapterCountPrototype GetAdapterCount => GetAdapterCountFunction ?? (GetAdapterCountFunction = StdCall.Create<GetAdapterCountPrototype>(this.GetAdapterCountPtr));

        #endregion
    }
}
