﻿using System;
using System.Runtime.InteropServices;

namespace MindWorX.Unmanaged.DirectX.Direct3D9
{
    [StructLayout(LayoutKind.Sequential)]
    // ReSharper disable once InconsistentNaming
    public struct IDirect3DDevice9VirtualTable
    {
        #region IUnknown

        public IntPtr QueryInterface;
        public IntPtr AddRef;
        public IntPtr Release;

        #endregion

        #region IDirect3DDevice9

        public IntPtr TestCooperativeLevel;
        public IntPtr GetAvailableTextureMem;
        public IntPtr EvictManagedResources;
        public IntPtr GetDirect3D;
        public IntPtr GetDeviceCaps;
        public IntPtr GetDisplayMode;
        public IntPtr GetCreationParameters;
        public IntPtr SetCursorProperties;
        public IntPtr SetCursorPosition;
        public IntPtr ShowCursor;
        public IntPtr CreateAdditionalSwapChain;
        public IntPtr GetSwapChain;
        public IntPtr GetNumberOfSwapChains;
        public IntPtr Reset;
        public IntPtr Present;
        public IntPtr GetBackBuffer;
        public IntPtr GetRasterStatus;
        public IntPtr SetDialogBoxMode;
        public IntPtr SetGammaRamp;
        public IntPtr GetGammaRamp;
        public IntPtr CreateTexture;
        public IntPtr CreateVolumeTexture;
        public IntPtr CreateCubeTexture;
        public IntPtr CreateVertexBuffer;
        public IntPtr CreateIndexBuffer;
        public IntPtr CreateRenderTarget;
        public IntPtr CreateDepthStencilSurface;
        public IntPtr UpdateSurface;
        public IntPtr UpdateTexture;
        public IntPtr GetRenderTargetData;
        public IntPtr GetFrontBufferData;
        public IntPtr StretchRect;
        public IntPtr ColorFill;
        public IntPtr CreateOffscreenPlainSurface;
        public IntPtr SetRenderTarget;
        public IntPtr GetRenderTarget;
        public IntPtr SetDepthStencilSurface;
        public IntPtr GetDepthStencilSurface;
        public IntPtr BeginScene;
        public IntPtr EndScene;
        public IntPtr Clear;
        public IntPtr SetTransform;
        public IntPtr GetTransform;
        public IntPtr MultiplyTransform;
        public IntPtr SetViewport;
        public IntPtr GetViewport;
        public IntPtr SetMaterial;
        public IntPtr GetMaterial;
        public IntPtr SetLight;
        public IntPtr GetLight;
        public IntPtr LightEnable;
        public IntPtr GetLightEnable;
        public IntPtr SetClipPlane;
        public IntPtr GetClipPlane;
        public IntPtr SetRenderState;
        public IntPtr GetRenderState;
        public IntPtr CreateStateBlock;
        public IntPtr BeginStateBlock;
        public IntPtr EndStateBlock;
        public IntPtr SetClipStatus;
        public IntPtr GetClipStatus;
        public IntPtr GetTexture;
        public IntPtr SetTexture;
        public IntPtr GetTextureStageState;
        public IntPtr SetTextureStageState;
        public IntPtr GetSamplerState;
        public IntPtr SetSamplerState;
        public IntPtr ValidateDevice;
        public IntPtr SetPaletteEntries;
        public IntPtr GetPaletteEntries;
        public IntPtr SetCurrentTexturePalette;
        public IntPtr GetCurrentTexturePalette;
        public IntPtr SetScissorRect;
        public IntPtr GetScissorRect;
        public IntPtr SetSoftwareVertexProcessing;
        public IntPtr GetSoftwareVertexProcessing;
        public IntPtr SetNPatchMode;
        public IntPtr GetNPatchMode;
        public IntPtr DrawPrimitive;
        public IntPtr DrawIndexedPrimitive;
        public IntPtr DrawPrimitiveUP;
        public IntPtr DrawIndexedPrimitiveUP;
        public IntPtr ProcessVertices;
        public IntPtr CreateVertexDeclaration;
        public IntPtr SetVertexDeclaration;
        public IntPtr GetVertexDeclaration;
        public IntPtr SetFVF;
        public IntPtr GetFVF;
        public IntPtr CreateVertexShader;
        public IntPtr SetVertexShader;
        public IntPtr GetVertexShader;
        public IntPtr SetVertexShaderConstantF;
        public IntPtr GetVertexShaderConstantF;
        public IntPtr SetVertexShaderConstantI;
        public IntPtr GetVertexShaderConstantI;
        public IntPtr SetVertexShaderConstantB;
        public IntPtr GetVertexShaderConstantB;
        public IntPtr SetStreamSource;
        public IntPtr GetStreamSource;
        public IntPtr SetStreamSourceFreq;
        public IntPtr GetStreamSourceFreq;
        public IntPtr SetIndices;
        public IntPtr GetIndices;
        public IntPtr CreatePixelShader;
        public IntPtr SetPixelShader;
        public IntPtr GetPixelShader;
        public IntPtr SetPixelShaderConstantF;
        public IntPtr GetPixelShaderConstantF;
        public IntPtr SetPixelShaderConstantI;
        public IntPtr GetPixelShaderConstantI;
        public IntPtr SetPixelShaderConstantB;
        public IntPtr GetPixelShaderConstantB;
        public IntPtr DrawRectPatch;
        public IntPtr DrawTriPatch;
        public IntPtr DeletePatch;
        public IntPtr CreateQuery;

        #endregion
    }
}
