﻿using System;
using System.Dynamic;
using MindWorX.Unmanaged.Windows;

namespace MindWorX.Unmanaged
{
    public class DynamicModule : DynamicObject
    {
        public DynamicModule(string module)
        {
            if (String.IsNullOrEmpty(module))
                throw new ArgumentNullException(nameof(module));

            this.Handle = Kernel32.GetModuleHandle(module);

            if (this.Handle == IntPtr.Zero)
                this.Handle = Kernel32.LoadLibrary(module);

            if (this.Handle == IntPtr.Zero)
                throw new ArgumentException("Could not locate the specified module.", nameof(module));
        }

        public DynamicModule(IntPtr handle)
        {
            if (handle == IntPtr.Zero)
                throw new ArgumentNullException(nameof(handle));
            this.Handle = handle;
        }

        public IntPtr Handle { get; }

        public IntPtr this[string name] => Kernel32.GetProcAddress(this.Handle, name);

        public IntPtr this[int offset] => this.Handle + offset;

        public bool TryGetProc(string name, out object result)
        {
            result = Kernel32.GetProcAddress(this.Handle, name);
            return (IntPtr)result != IntPtr.Zero;
        }

        public bool TryGetProc(string name, out IntPtr result)
        {
            result = Kernel32.GetProcAddress(this.Handle, name);
            return result != IntPtr.Zero;
        }

        public bool TryGetSub(int offset, out object result)
        {
            result = this.Handle + offset;
            return (IntPtr)result != IntPtr.Zero;
        }

        public bool TryGetSub(int offset, out IntPtr result)
        {
            result = this.Handle + offset;
            return result != IntPtr.Zero;
        }

        public bool TryGetOrdinal(int ordinal, out object result)
        {
            result = Kernel32.GetProcAddress(this.Handle, ordinal);
            return (IntPtr)result != IntPtr.Zero;
        }

        public bool TryGetOrdinal(int ordinal, out IntPtr result)
        {
            result = Kernel32.GetProcAddress(this.Handle, ordinal);
            return result != IntPtr.Zero;
        }

        public IntPtr GetProc(string name) => Kernel32.GetProcAddress(this.Handle, name);

        public IntPtr GetSub(int offset) => this.Handle + offset;

        public IntPtr GetOrdinal(int ordinal) => Kernel32.GetProcAddress(this.Handle, ordinal);

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            if (binder.Name.StartsWith("Ord_"))
                return this.TryGetOrdinal(int.Parse(binder.Name.Substring(4)), out result);

            if (binder.Name.StartsWith("Sub_"))
                return this.TryGetSub(Convert.ToInt32(binder.Name.Substring(4), 16), out result);

            return this.TryGetProc(binder.Name, out result);
        }
    }
}
