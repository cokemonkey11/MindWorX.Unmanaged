﻿using System;
using System.Runtime.InteropServices;
using Binarysharp.Assemblers.Fasm;
using MindWorX.Unmanaged.Windows;

namespace MindWorX.Unmanaged
{
    public static class FasmNetExtensions
    {
        public static IntPtr AssembleToExecutable(this FasmNet fasm)
        {
            var code = fasm.Assemble();
            var codePtr = Kernel32.VirtualAlloc(code.Length, Mem.Commit, Page.ExecuteReadWrite);
            Marshal.Copy(code, 0, codePtr, code.Length);
            Kernel32.VirtualProtect(codePtr, code.Length, Page.ExecuteRead);
            return codePtr;
        }

        public static T AssembleToDelegate<T>(this FasmNet fasm) => Marshal.GetDelegateForFunctionPointer<T>(fasm.AssembleToExecutable());
    }
}
