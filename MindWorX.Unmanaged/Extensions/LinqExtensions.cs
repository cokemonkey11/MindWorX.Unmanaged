﻿using System.Collections.Generic;

namespace MindWorX.Unmanaged
{
    internal static class LinqExtensions
    {
        public static IEnumerable<T> Append<T>(this IEnumerable<T> collection, params T[] items)
        {
            foreach (var item in collection)
                yield return item;
            foreach (var item in items)
                yield return item;
        }
        public static IEnumerable<T> Prepend<T>(this IEnumerable<T> collection, params T[] items)
        {
            foreach (var item in items)
                yield return item;
            foreach (var item in collection)
                yield return item;
        }
    }
}
