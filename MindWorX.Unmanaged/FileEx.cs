﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using Microsoft.Win32.SafeHandles;
using MindWorX.Unmanaged.Windows;

namespace MindWorX.Unmanaged
{
    public static class FileEx
    {
        [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        private static extern SafeFileHandle CreateFile(
            string lpFileName,
            [MarshalAs(UnmanagedType.U4)] FileAccess dwDesiredAccess,
            [MarshalAs(UnmanagedType.U4)] FileShare dwShareMode,
            IntPtr lpSecurityAttributes,
            [MarshalAs(UnmanagedType.U4)] FileMode dwCreationDisposition,
            [MarshalAs(UnmanagedType.U4)] FileAttributes dwFlagsAndAttributes,
            IntPtr hTemplateFile);

        public static FileStream Open(string path, FileMode mode, FileAccess access, FileShare share)
            => new FileStream(CreateFile(path, access, share, IntPtr.Zero, mode, 0, IntPtr.Zero), access);

        public static FileStream Open(string path, FileMode mode, FileAccess access) => Open(path, mode, access, FileShare.Read);

        public static FileStream Open(string path, FileMode mode) => Open(path, mode, FileAccess.Read, FileShare.Read);

        public static SecurityZone GetSecurityZone(string path)
        {
            var zoneId = new StringBuilder(255);
            Kernel32.GetPrivateProfileString("ZoneTransfer", "ZoneId", "-1", zoneId, zoneId.Capacity, path + ":Zone.Identifier");
            return (SecurityZone)Enum.Parse(typeof(SecurityZone), zoneId.ToString());
        }

        public static bool RemoveSecurityZone(string path)
        {
            return Kernel32.WritePrivateProfileString("ZoneTransfer", "ZoneId", null, path + ":Zone.Identifier") != 0;
        }

        public static bool IsBlocked(string path)
        {
            var zone = GetSecurityZone(path);
            return zone == SecurityZone.Internet || zone == SecurityZone.Untrusted;
        }
    }
}
