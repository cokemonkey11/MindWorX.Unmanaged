﻿using System;
using System.Linq;
using System.Reflection.Emit;
using System.Runtime.InteropServices;
using Binarysharp.Assemblers.Fasm;

namespace MindWorX.Unmanaged.Invokers
{
    public static class FastCall
    {
        private static readonly IntPtr TransformStdCallToFastCallPtr;

        static FastCall()
        {
            var fasm = new FasmNet();

            fasm.AddLine("use32");
            fasm.AddLine("pop   edx");
            fasm.AddLine("xchg  edx, [esp+8]");
            fasm.AddLine("pop   eax");
            fasm.AddLine("pop   ecx");
            fasm.AddLine("jmp   eax");

            TransformStdCallToFastCallPtr = fasm.AssembleToExecutable();
        }

        public static IntPtr WrapStdCall(IntPtr functionPtr)
        {
            var fasm = new FasmNet();

            fasm.AddLine("use32");
            fasm.AddLine("pop   ebx");      // pop the return pointer
            fasm.AddLine("push  edx");      // push our info
            fasm.AddLine("push  ecx");      // push our info
            fasm.AddLine("mov   eax, {0}", functionPtr);
            fasm.AddLine("call  eax");      // call the wrapped function
            fasm.AddLine("push  ebx");      // push the correct return pointer
            fasm.AddLine("retn");           // return

            return fasm.AssembleToExecutable();
        }

        public static IntPtr WrapStdCall<T>(T functionPtr) where T : class
        {
            if (!typeof(T).IsSubclassOf(typeof(Delegate)))
                throw new ArgumentException($"Invalid generic type {typeof(T).Name}.", nameof(T));
            return WrapStdCall(Marshal.GetFunctionPointerForDelegate(functionPtr));
        }

        public static DynamicMethod Create(IntPtr address, Type returnType, params Type[] parameterTypes)
        {
            string name;

            if (parameterTypes.Any())
                name = $"{returnType.Name} {nameof(FastCall)}_{address.ToString("X8")}({parameterTypes.Select(t => t.Name).Aggregate((a, b) => a + ", " + b)})";
            else
                name = $"{returnType.Name} {nameof(FastCall)}_{address.ToString("X8")}()";

            var method = new DynamicMethod(name, returnType, parameterTypes, typeof(FastCall).Module);

            var il = method.GetILGenerator();
            // push the address to wrap first
            il.Emit(OpCodes.Ldc_I4, (int)address);
            il.Emit(OpCodes.Conv_I);
            // push the parameters
            for (var i = 0; i < parameterTypes.Length; i += 1)
                il.Emit(OpCodes.Ldarg, i);
            // push the address to call
            il.Emit(OpCodes.Ldc_I4, (int)TransformStdCallToFastCallPtr);
            il.Emit(OpCodes.Conv_I);
            // call
            il.EmitCalli(OpCodes.Calli, CallingConvention.StdCall, returnType, parameterTypes.Prepend(typeof(IntPtr)).ToArray());
            il.Emit(OpCodes.Ret);

            return method;
        }

        public static T Create<T>(IntPtr address) where T : class
        {
            var delegateType = typeof(T);

            if (!delegateType.IsSubclassOf(typeof(Delegate)))
                throw new ArgumentException($"Invalid generic type {delegateType.Name}.", nameof(T));

            var returnType = delegateType.GetMethod("Invoke").ReturnType;
            var parameterTypes = delegateType.GetMethod("Invoke").GetParameters().Select(t => t.ParameterType).ToArray();

            return (T)(object)Create(address, returnType, parameterTypes).CreateDelegate(typeof(T));
        }

        public static void Invoke(IntPtr address, params object[] parameters)
        {
            var returnType = typeof(void);
            var parameterTypes = parameters.Select(o => o.GetType()).ToArray();

            Create(address, returnType, parameterTypes).Invoke(null, parameters);
        }

        public static T Invoke<T>(IntPtr address, params object[] parameters) where T : struct
        {
            var returnType = typeof(T);
            var parameterTypes = parameters.Select(o => o.GetType()).ToArray();

            return (T)Create(address, returnType, parameterTypes).Invoke(null, parameters);
        }
    }
}
