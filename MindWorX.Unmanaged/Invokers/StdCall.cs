﻿using System;
using System.Linq;
using System.Reflection.Emit;
using System.Runtime.InteropServices;

namespace MindWorX.Unmanaged.Invokers
{
    public static class StdCall
    {
        public static DynamicMethod Create(IntPtr address, Type returnType, params Type[] parameterTypes)
        {
            string name;

            if (parameterTypes.Any())
                name = $"{returnType.Name} {nameof(StdCall)}_{address.ToString("X8")}({parameterTypes.Select(t => t.Name).Aggregate((a, b) => a + ", " + b)})";
            else
                name = $"{returnType.Name} {nameof(StdCall)}_{address.ToString("X8")}()";

            var method = new DynamicMethod(name, returnType, parameterTypes, typeof(StdCall).Module);

            var il = method.GetILGenerator();
            // push the parameters
            for (var i = 0; i < parameterTypes.Length; i += 1)
                il.Emit(OpCodes.Ldarg, i);
            // push the address to call
            il.Emit(OpCodes.Ldc_I4, (int)address);
            il.Emit(OpCodes.Conv_I);
            // call
            il.EmitCalli(OpCodes.Calli, CallingConvention.StdCall, returnType, parameterTypes);
            il.Emit(OpCodes.Ret);

            return method;
        }

        public static T Create<T>(IntPtr address) where T : class
        {
            var delegateType = typeof(T);

            if (!delegateType.IsSubclassOf(typeof(Delegate)))
                throw new ArgumentException($"Invalid generic type {delegateType.Name}, must be a delegate.", nameof(T));

            var returnType = delegateType.GetMethod("Invoke").ReturnType;
            var parameterTypes = delegateType.GetMethod("Invoke").GetParameters().Select(t => t.ParameterType).ToArray();

            return (T)(object)Create(address, returnType, parameterTypes).CreateDelegate(typeof(T));
        }

        public static void Invoke(IntPtr address, params object[] parameters)
        {
            var returnType = typeof(void);
            var parameterTypes = parameters.Select(o => o.GetType()).ToArray();

            Create(address, returnType, parameterTypes).Invoke(null, parameters);
        }

        public static T Invoke<T>(IntPtr address, params object[] parameters) where T : struct
        {
            var returnType = typeof(T);
            var parameterTypes = parameters.Select(o => o.GetType()).ToArray();

            return (T)Create(address, returnType, parameterTypes).Invoke(null, parameters);
        }
    }
}