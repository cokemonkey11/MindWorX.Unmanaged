﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using MindWorX.Unmanaged.Windows;

namespace MindWorX.Unmanaged
{
    public static class Memory
    {
        public static T Read<T>(IntPtr address) where T : struct => Marshal.PtrToStructure<T>(address);

        public static T Read<T>(IntPtr address, int offset) where T : struct => Marshal.PtrToStructure<T>(address + offset);

        public static void Read(IntPtr address, byte[] buffer, int startIndex, int length) => Marshal.Copy(address, buffer, 0, length);

        public static void Read(IntPtr address, byte[] buffer, int startIndex, int length, bool force)
        {
            if (force)
            {
                Page old;
                Kernel32.VirtualProtect(address, length, Page.ExecuteReadWrite, out old);
                Read(address, buffer, startIndex, length);
                Kernel32.VirtualProtect(address, length, old, out old); // Restore
            }
            else
            {
                Read(address, buffer, startIndex, length);
            }
        }

        public static void Write<T>(IntPtr address, T data) where T : struct => Marshal.StructureToPtr(data, address, true);

        public static void Write<T>(IntPtr address, int offset, T data) where T : struct => Write(address + offset, data);

        public static void Write(IntPtr address, byte[] buffer) => Marshal.Copy(buffer, 0, address, buffer.Length);

        public static void Write(IntPtr address, byte[] buffer, bool force)
        {
            if (force)
            {
                Page old;
                Kernel32.VirtualProtect(address, buffer.Length, Page.ExecuteReadWrite, out old);
                Write(address, buffer);
                Kernel32.VirtualProtect(address, buffer.Length, old, out old); // Restore
            }
            else
            {
                Write(address, buffer);
            }
        }

        public static IntPtr AllocString(string data) => Marshal.StringToHGlobalAnsi(data);

        public static string ReadString(IntPtr address) => Marshal.PtrToStringAnsi(address);

        public static string ReadString(IntPtr address, int length) => Marshal.PtrToStringAnsi(address, length);

        public static void WriteString(IntPtr address, string data) =>
            Write(address, new List<byte>(Encoding.ASCII.GetBytes(data)) { 0x00 }.ToArray());

        public static void WriteString(IntPtr address, string data, bool force) =>
            Write(address, new List<byte>(Encoding.ASCII.GetBytes(data)) { 0x00 }.ToArray(), force);
    }
}
