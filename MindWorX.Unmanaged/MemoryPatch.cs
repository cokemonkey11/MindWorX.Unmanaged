﻿using System;

namespace MindWorX.Unmanaged
{
    public class MemoryPatch
    {
        private readonly bool force;

        private bool isEnabled;

        public MemoryPatch(IntPtr address, bool force, params byte[] newPatch)
        {
            this.Address = address;
            this.force = force;
            this.NewPatch = newPatch;
            this.OldPatch = new byte[this.NewPatch.Length];
            Memory.Read(address, this.OldPatch, 0, this.OldPatch.Length, true);
        }

        public MemoryPatch(IntPtr address, params byte[] newPatch)
            : this(address, false, newPatch)
        { }

        public IntPtr Address { get; }

        public byte[] OldPatch { get; }

        public byte[] NewPatch { get; }

        public bool IsEnabled
        {
            get { return this.isEnabled; }
            set
            {
                if (value == this.isEnabled)
                    return;
                this.isEnabled = value;
                if (this.isEnabled)
                    this.Enable();
                else
                    this.Disable();
            }
        }

        private void Enable() => Memory.Write(this.Address, this.NewPatch, this.force);

        private void Disable() => Memory.Write(this.Address, this.OldPatch, this.force);
    }
}
