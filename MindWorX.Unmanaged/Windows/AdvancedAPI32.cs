﻿using System;
using System.Runtime.InteropServices;

namespace MindWorX.Unmanaged.Windows
{
    public static unsafe class AdvancedAPI32
    {
        public const string LIBRARY_NAME = "advapi32.dll";

        public static IntPtr Handle
        {
            get
            {
                var handle = Kernel32.GetModuleHandle(LIBRARY_NAME);
                return handle != IntPtr.Zero ? handle : Kernel32.LoadLibrary(LIBRARY_NAME);
            }
        }

        #region RegCreateKeyExA

        public delegate int RegCreateKeyExAPrototype([In] UIntPtr hKey, [In] string lpSubKey, [In] int reserved, [In, Optional] string lpClass, [In] REG_OPTION dwOptions,
            [In] REGSAM samDesired, [In, Optional] ref SECURITY_ATTRIBUTES lpSecurityAttributes, [Out] out UIntPtr phkResult, [Out, Optional] out REG lpdwDisposition);

        [DllImport(LIBRARY_NAME, CharSet = CharSet.Ansi, SetLastError = true)]
        public static extern int RegCreateKeyExA([In] UIntPtr hKey, [In] string lpSubKey, [In] int reserved, [In, Optional] string lpClass, [In] REG_OPTION dwOptions,
            [In] REGSAM samDesired, [In, Optional] ref SECURITY_ATTRIBUTES lpSecurityAttributes, [Out] out UIntPtr phkResult, [Out, Optional] out REG lpdwDisposition);

        #endregion

        #region RegOpenKeyExA

        public delegate int RegOpenKeyExAPrototype([In] UIntPtr hKey, [In, Optional] string lpSubKey, [In] REG_OPTION ulOptions,
            [In] REGSAM samDesired, [Out] out UIntPtr phkResult);

        [DllImport(LIBRARY_NAME, CharSet = CharSet.Ansi, SetLastError = true)]
        public static extern int RegOpenKeyExA([In] UIntPtr hKey, [In, Optional] string lpSubKey, [In] REG_OPTION ulOptions,
            [In] REGSAM samDesired, [Out] out UIntPtr phkResult);

        #endregion

        #region RegQueryValueExA

        public delegate int RegQueryValueExAPrototype([In] UIntPtr hKey, [In, Optional] string lpValueName, [In] int reserved,
            [Out, Optional] REG_TYPE* lpType, [Out, Optional] byte* lpData, [In, Out, Optional] int* lpcbData);

        [DllImport(LIBRARY_NAME, CharSet = CharSet.Ansi, SetLastError = true)]
        public static extern int RegQueryValueExA([In] UIntPtr hKey, [In, Optional] string lpValueName, [In] int reserved,
            [Out, Optional] REG_TYPE* lpType, [Out, Optional] byte* lpData, [In, Out, Optional] int* lpcbData);

        #endregion

        #region RegSetValueEx

        public delegate int RegSetValueExPrototype([In] UIntPtr hKey, [In, Optional] string lpValueName, [In] int reserved,
            [In] REG_TYPE lpType, [In] IntPtr lpData, [In] ref int cbData);

        [DllImport(LIBRARY_NAME, CharSet = CharSet.Ansi, SetLastError = true)]
        public static extern int RegSetValueEx([In] UIntPtr hKey, [In, Optional] string lpValueName, [In] int reserved,
            [In] REG_TYPE lpType, [In] IntPtr lpData, [In] ref int cbData);

        #endregion
    }
}
