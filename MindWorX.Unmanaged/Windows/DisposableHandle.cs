﻿using System;

namespace MindWorX.Unmanaged.Windows
{
    public class DisposableHandle : IDisposable
    {
        private readonly Action<IntPtr> disposer;

        public DisposableHandle(IntPtr handle, Action<IntPtr> disposer)
        {
            if (disposer == null)
                throw new ArgumentNullException(nameof(disposer));

            this.Handle = handle;
            this.disposer = disposer;
        }

        public IntPtr Handle { get; }

        public void Dispose() => this.disposer(this.Handle);
    }
}
