﻿namespace MindWorX.Unmanaged.Windows
{
    public enum GWL : int
    {
        ExStyle = -20,
        HInstance = -6,
        Id = -12,
        Style = -16,
        UserData = -21,
        WndProc = -4,
    }
}
