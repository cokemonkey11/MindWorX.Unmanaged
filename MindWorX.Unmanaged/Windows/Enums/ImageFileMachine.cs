﻿namespace MindWorX.Unmanaged.Windows
{
    public enum ImageFileMachine : ushort
    {
        I386 = 0x014C,
        IA64 = 0x0200,
        AMD64 = 0x8664,
    }
}
