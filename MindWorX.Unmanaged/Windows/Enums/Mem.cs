﻿using System;

namespace MindWorX.Unmanaged.Windows
{
    [Flags]
    public enum Mem
    {
        Commit = 0x00001000,
        Reserve = 0x00002000,
        Decommit = 0x00004000,
        Release = 0x00008000,

        Reset = 0x00080000,
        ResetUndo = 0x01000000,

        TopDown = 0x00100000,
        WriteWatch = 0x00200000,
        Physical = 0x00400000,

        Image = 0x01000000,
        Mapped = 0x00040000,
        Private = 0x00020000,

        LargePages = 0x20000000,
    }
}
