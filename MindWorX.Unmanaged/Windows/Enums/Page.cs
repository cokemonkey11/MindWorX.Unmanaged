﻿using System;

namespace MindWorX.Unmanaged.Windows
{
    /// <summary>
    /// The following are the memory-protection options; you must specify one of the following values when allocating or protecting a page in memory.
    /// Protection attributes cannot be assigned to a portion of a page; they can only be assigned to a whole page.
    /// <remarks>https://msdn.microsoft.com/en-us/library/windows/desktop/aa366786(v=vs.85).aspx</remarks>
    /// </summary>
    [Flags]
    public enum Page : uint
    {
        /// <summary>
        /// Disables all access to the committed region of pages. An attempt to read from, write to, or execute the committed region results in an access violation.
        /// This flag is not supported by the CreateFileMapping function.
        /// </summary>
        NoAccess = 0x01,

        /// <summary>
        /// Enables read-only access to the committed region of pages. An attempt to write to the committed region results in an access violation. 
        /// If Data Execution Prevention is enabled, an attempt to execute code in the committed region results in an access violation.
        /// </summary>
        ReadOnly = 0x02,

        /// <summary>
        /// Enables read-only or read/write access to the committed region of pages. If Data Execution Prevention is enabled, attempting to execute code in the committed region results in an access violation.
        /// </summary>
        ReadWrite = 0x04,

        /// <summary>
        /// Enables read-only or copy-on-write access to a mapped view of a file mapping object.
        /// An attempt to write to a committed copy-on-write page results in a private copy of the page being made for the process. 
        /// The private page is marked as <see cref="ReadWrite"/>, and the change is written to the new page. 
        /// If Data Execution Prevention is enabled, attempting to execute code in the committed region results in an access violation.
        /// </summary>
        WriteCopy = 0x08,

        /// <summary>
        /// Enables execute access to the committed region of pages. An attempt to write to the committed region results in an access violation.
        /// This flag is not supported by the CreateFileMapping function.
        /// </summary>
        Execute = 0x10,

        /// <summary>
        /// Enables execute or read-only access to the committed region of pages. An attempt to write to the committed region results in an access violation.
        /// Windows Server 2003 and Windows XP:  This attribute is not supported by the CreateFileMapping function until Windows XP with SP2 and Windows Server 2003 with SP1.
        /// </summary>
        ExecuteRead = 0x20,

        /// <summary>
        /// Enables execute, read-only, or read/write access to the committed region of pages.
        /// Windows Server 2003 and Windows XP:  This attribute is not supported by the CreateFileMapping function until Windows XP with SP2 and Windows Server 2003 with SP1.
        /// </summary>
        ExecuteReadWrite = 0x40,

        /// <summary>
        /// Enables execute, read-only, or copy-on-write access to a mapped view of a file mapping object. 
        /// An attempt to write to a committed copy-on-write page results in a private copy of the page being made for the process.
        /// The private page is marked as <see cref="ExecuteReadWrite"/>, and the change is written to the new page.
        /// This flag is not supported by the <see cref="Kernel32.VirtualAlloc"/> or <see cref="Kernel32.VirtualAllocEx"/> functions. 
        /// Windows Vista, Windows Server 2003, and Windows XP:  This attribute is not supported by the CreateFileMapping function until Windows Vista with SP1 and Windows Server 2008.
        /// </summary>
        ExecuteWriteCopy = 0x80,

        /// <summary>
        /// Pages in the region become guard pages. Any attempt to access a guard page causes the system to raise a STATUS_GUARD_PAGE_VIOLATION exception and turn off the guard page status. 
        /// Guard pages thus act as a one-time access alarm. For more information, see Creating Guard Pages.
        /// When an access attempt leads the system to turn off guard page status, the underlying page protection takes over.
        /// If a guard page exception occurs during a system service, the service typically returns a failure status indicator.
        /// This value cannot be used with <see cref="NoAccess"/>.
        /// </summary>
        Guard = 0x100,

        /// <summary>
        /// 
        /// </summary>
        NoCache = 0x200,

        /// <summary>
        /// 
        /// </summary>
        WriteCombine = 0x400,

        /// <summary>
        /// Sets all locations in the pages as invalid targets for CFG.
        /// Used along with any execute page protection like <see cref="Execute"/>, <see cref="ExecuteRead"/>, <see cref="ExecuteReadWrite"/> and <see cref="ExecuteWriteCopy"/>. 
        /// Any indirect call to locations in those pages will fail CFG checks and the process will be terminated. The default behavior for executable pages allocated is to be marked valid call targets for CFG.
        /// </summary>
        TargetsInvalid = 0x40000000,

        /// <summary>
        /// Pages in the region will not have their CFG information updated while the protection changes for VirtualProtect.
        /// For example, if the pages in the region was allocated using <see cref="TargetsInvalid"/>, then the invalid information will be maintained while the page protection changes.
        /// This flag is only valid when the protection changes to an executable type like <see cref="Execute"/>, <see cref="ExecuteRead"/>, <see cref="ExecuteReadWrite"/> and <see cref="ExecuteWriteCopy"/>.
        /// The default behavior for VirtualProtect protection change to executable is to mark all locations as valid call targets for CFG. 
        /// </summary>
        TargetsNoUpdate = 0x40000000
    }
}
