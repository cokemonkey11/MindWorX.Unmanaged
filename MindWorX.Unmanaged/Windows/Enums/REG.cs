﻿// ReSharper disable InconsistentNaming

namespace MindWorX.Unmanaged.Windows
{
    public enum REG
    {
        CREATED_NEW_KEY = 0x00000001,
        OPENED_EXISTING_KEY = 0x00000002
    }
}
