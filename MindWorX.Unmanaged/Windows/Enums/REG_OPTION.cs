﻿// ReSharper disable InconsistentNaming

using System;

namespace MindWorX.Unmanaged.Windows
{
    [Flags]
    public enum REG_OPTION
    {
        NON_VOLATILE = 0x00000000,
        VOLATILE = 0x00000001,
        CREATE_LINK = 0x00000002,
        BACKUP_RESTORE = 0x00000004,
        OPEN_LINK = 0x00000008
    }
}
