﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace MindWorX.Unmanaged.Windows
{
    public static class Kernel32
    {
        public const string LIBRARY_NAME = "kernel32.dll";

        public static IntPtr Handle
        {
            get
            {
                var handle = GetModuleHandle(LIBRARY_NAME);
                return handle != IntPtr.Zero ? handle : LoadLibrary(LIBRARY_NAME);
            }
        }

        #region AttachConsole

        [DllImport(LIBRARY_NAME, SetLastError = true)]
        public static extern bool AttachConsole(int pw);

        #endregion

        #region AllocConsole

        [DllImport(LIBRARY_NAME, SetLastError = true)]
        public static extern bool AllocConsole();

        #endregion

        #region CopyMemory

        [DllImport(LIBRARY_NAME, SetLastError = true)]
        private static extern void CopyMemory(IntPtr destination, IntPtr source, int size);

        public static void CopyMemory(IntPtr destination, byte[] source)
        {
            unsafe
            {
                fixed (byte* pSource = source)
                    CopyMemory(destination, (IntPtr)pSource, source.Length);
            }
        }

        #endregion

        [UnmanagedFunctionPointer(CallingConvention.StdCall, SetLastError = true, CharSet = CharSet.Ansi)]
        public delegate bool CreateProcessA_SafePrototype(IntPtr lpApplicationName, IntPtr lpCommandLine, IntPtr lpProcessAttributes, IntPtr lpThreadAttributes, bool bInheritHandles, uint dwCreationFlags, IntPtr lpEnvironment, IntPtr lpCurrentDirectory, IntPtr lpStartupInfo, IntPtr lpProcessInformation);

        [DllImport(LIBRARY_NAME, EntryPoint = "CreateProcessA", SetLastError = true, CharSet = CharSet.Ansi)]
        public static extern bool CreateProcessA_Safe(IntPtr lpApplicationName, IntPtr lpCommandLine, IntPtr lpProcessAttributes, IntPtr lpThreadAttributes, bool bInheritHandles, uint dwCreationFlags, IntPtr lpEnvironment, IntPtr lpCurrentDirectory, IntPtr lpStartupInfo, IntPtr lpProcessInformation);

        [DllImport(LIBRARY_NAME, EntryPoint = "CreateProcessA", SetLastError = true, CharSet = CharSet.Ansi)]
        public static extern unsafe bool CreateProcessA(
            [In, Optional] StringBuilder lpApplicationName,
            [In, Out] ref StringBuilder lpCommandLine,
            [In, Optional] SECURITY_ATTRIBUTES* lpProcessAttributes,
            [In, Optional] SECURITY_ATTRIBUTES* lpThreadAttributes,
            [In] bool bInheritHandles,
            [In] ProcessCreationFlags dwCreationFlags,
            [In, Optional] void* lpEnvironment,
            [In, Optional] StringBuilder lpCurrentDirectory,
            [In] ref STARTUPINFO lpStartupInfo,
            [Out] out PROCESS_INFORMATION lpProcessInformation);

        [DllImport(LIBRARY_NAME, EntryPoint = "CreateProcessA", SetLastError = true, CharSet = CharSet.Ansi)]
        public static extern unsafe bool CreateProcessA(
            [In, Optional] StringBuilder lpApplicationName,
            [In, Optional] StringBuilder lpCommandLine,
            [In, Optional] SECURITY_ATTRIBUTES* lpProcessAttributes,
            [In, Optional] SECURITY_ATTRIBUTES* lpThreadAttributes,
            [In] bool bInheritHandles,
            [In] ProcessCreationFlags dwCreationFlags,
            [In, Optional] void* lpEnvironment,
            [In, Optional] StringBuilder lpCurrentDirectory,
            [In] ref STARTUPINFO lpStartupInfo,
            [Out] out PROCESS_INFORMATION lpProcessInformation);

        [DllImport(LIBRARY_NAME, EntryPoint = "CreateProcessA", SetLastError = true, CharSet = CharSet.Ansi)]
        public static extern unsafe bool CreateProcessA(
            [In, Optional] string lpApplicationName,
            [In, Optional] string lpCommandLine,
            [In, Optional] SECURITY_ATTRIBUTES* lpProcessAttributes,
            [In, Optional] SECURITY_ATTRIBUTES* lpThreadAttributes,
            [In] bool bInheritHandles,
            [In] ProcessCreationFlags dwCreationFlags,
            [In, Optional] void* lpEnvironment,
            [In, Optional] string lpCurrentDirectory,
            [In] ref STARTUPINFO lpStartupInfo,
            [Out] out PROCESS_INFORMATION lpProcessInformation);

        #region FreeConsole

        [DllImport(LIBRARY_NAME, SetLastError = true)]
        public static extern bool FreeConsole();

        #endregion

        #region GetModuleHandle

        [DllImport(LIBRARY_NAME, CharSet = CharSet.Ansi, SetLastError = true)]
        public static extern IntPtr GetModuleHandle(string name);

        #endregion

        [DllImport(LIBRARY_NAME, CharSet = CharSet.Unicode)]
        public static extern int GetPrivateProfileString(string section, string key, string fallback, StringBuilder retVal, int size, string filePath);

        #region GetProcAddress

        [DllImport(LIBRARY_NAME, CharSet = CharSet.Ansi, SetLastError = true)]
        public static extern IntPtr GetProcAddress(IntPtr module, string name);

        [DllImport(LIBRARY_NAME, CharSet = CharSet.Ansi, SetLastError = true)]
        public static extern IntPtr GetProcAddress(IntPtr module, int ordinal);

        #endregion

        #region LoadLibrary

        [UnmanagedFunctionPointer(CallingConvention.Winapi, CharSet = CharSet.Auto, SetLastError = true)]
        public delegate IntPtr LoadLibraryPrototype(string filename);

        [DllImport(LIBRARY_NAME, CharSet = CharSet.Auto, SetLastError = true)]
        public static extern IntPtr LoadLibrary(string filename);

        [UnmanagedFunctionPointer(CallingConvention.Winapi, CharSet = CharSet.Ansi, SetLastError = true)]
        public delegate IntPtr LoadLibraryAPrototype(string filename);

        [DllImport(LIBRARY_NAME, CharSet = CharSet.Ansi, SetLastError = true)]
        public static extern IntPtr LoadLibraryA(string filename);

        [UnmanagedFunctionPointer(CallingConvention.Winapi, CharSet = CharSet.Unicode, SetLastError = true)]
        public delegate IntPtr LoadLibraryWPrototype(string filename);

        [DllImport(LIBRARY_NAME, CharSet = CharSet.Unicode, SetLastError = true)]
        public static extern IntPtr LoadLibraryW(string filename);

        #endregion

        #region VirtualAlloc

        [DllImport(LIBRARY_NAME, SetLastError = true)]
        public static extern IntPtr VirtualAlloc(IntPtr address, int size, Mem allocationType, Page protect);

        public static IntPtr VirtualAlloc(int size, Mem allocationType, Page protect) => VirtualAlloc(IntPtr.Zero, size, allocationType, protect);

        #endregion VirtualAlloc

        #region VirtualAllocEx

        [DllImport(LIBRARY_NAME, SetLastError = true, ExactSpelling = true)]
        public static extern IntPtr VirtualAllocEx(IntPtr processHandle, IntPtr address, int size, Mem allocationType, Page protect);

        public static IntPtr VirtualAllocEx(IntPtr processHandle, int size, Mem allocationType, Page protect) => VirtualAllocEx(processHandle, IntPtr.Zero, size, allocationType, protect);

        #endregion VirtualAllocEx

        #region VirtualFree

        [DllImport(LIBRARY_NAME, SetLastError = true)]
        public static extern bool VirtualFree(IntPtr address, int size, Mem freeType);

        #endregion VirtualFree

        #region VirtualProtect

        [DllImport(LIBRARY_NAME, SetLastError = true)]
        public static extern bool VirtualProtect(IntPtr address, int size, Page protect, out Page oldProtect);

        [DllImport(LIBRARY_NAME, SetLastError = true)]
        public static extern unsafe bool VirtualProtect(IntPtr address, int size, Page protect, Page* oldProtect);

        public static bool VirtualProtect(IntPtr address, int size, Page protect)
        {
            unsafe
            {
                return VirtualProtect(address, size, protect, null);
            }
        }

        #endregion VirtualProtect

        #region VirtualQuery

        [DllImport(LIBRARY_NAME, SetLastError = true)]
        public static extern unsafe int VirtualQuery(IntPtr address, MemoryBasicInformation* buffer, int size);

        [DllImport(LIBRARY_NAME, SetLastError = true)]
        public static extern int VirtualQuery(IntPtr address, ref MemoryBasicInformation buffer, int size);

        public static unsafe int VirtualQuery(IntPtr address, ref MemoryBasicInformation buffer) => VirtualQuery(address, ref buffer, sizeof(MemoryBasicInformation));

        #endregion

        [DllImport(LIBRARY_NAME, CharSet = CharSet.Unicode)]
        public static extern long WritePrivateProfileString(string section, string key, string value, string filePath);

        [DllImport(LIBRARY_NAME, SetLastError = true)]
        public static extern IntPtr OpenThread(ThreadAccess dwDesiredAccess, bool bInheritHandle, uint dwThreadId);

        public static DisposableHandle OpenDisposableThread(ThreadAccess dwDesiredAccess, bool bInheritHandle, uint dwThreadId)
            => new DisposableHandle(OpenThread(dwDesiredAccess, bInheritHandle, dwThreadId), handle => CloseHandle(handle));

        [DllImport(LIBRARY_NAME, SetLastError = true)]
        public static extern uint SuspendThread(IntPtr hThread);

        [DllImport(LIBRARY_NAME, SetLastError = true)]
        public static extern int ResumeThread(IntPtr hThread);

        [DllImport(LIBRARY_NAME, SetLastError = true)]
        public static extern bool CloseHandle(IntPtr hObject);

        [UnmanagedFunctionPointer(CallingConvention.StdCall, SetLastError = true, CharSet = CharSet.Ansi)]
        public delegate uint GetModuleFileNameAPrototype(
            [In, Optional] IntPtr hModule,
            [Out] IntPtr lpFilename,
            [In] uint nSize);

        [DllImport(LIBRARY_NAME, SetLastError = true, CharSet = CharSet.Ansi)]
        public static extern uint GetModuleFileNameA(
            [In, Optional] IntPtr hModule,
            [Out] IntPtr lpFilename,
            [In] uint nSize);
    }
}
