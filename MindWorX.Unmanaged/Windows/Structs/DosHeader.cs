﻿using System.Runtime.InteropServices;

namespace MindWorX.Unmanaged.Windows
{
    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct DosHeader
    {
        public short e_magic;

        public short e_cblp;

        public short e_cp;

        public short e_crlc;

        public short e_cparhdr;

        public short e_minalloc;

        public short e_maxalloc;

        public short e_ss;

        public short e_sp;

        public short e_csum;

        public short e_ip;

        public short e_cs;

        public short e_lfarlc;

        public short e_ovno;

        public fixed short e_res[4];

        public short e_oemid;

        public short e_oeminfo;

        public fixed short e_res2[10];

        public int e_lfanew;
    }
}
