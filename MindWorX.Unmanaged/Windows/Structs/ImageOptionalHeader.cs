﻿using System.Runtime.InteropServices;

namespace MindWorX.Unmanaged.Windows
{
    [StructLayout(LayoutKind.Sequential)]
    public struct ImageOptionalHeader
    {
        public short Magic;

        public byte MajorLinkerVersion;

        public byte MinorLinkerVersion;

        public int SizeOfCode;

        public int SizeOfInitializedData;

        public int SizeOfUninitializedData;

        public int AddressOfEntryPoint;

        public int BaseOfCode;

        public int BaseOfData;

        public int ImageBase;

        public int SectionAlignment;

        public int FileAlignment;

        public short MajorOperatingSystemVersion;

        public short MinorOperatingSystemVersion;

        public short MajorImageVersion;

        public short MinorImageVersion;

        public short MajorSubsystemVersion;

        public short MinorSubsystemVersion;

        public int Win32VersionValue;

        public int SizeOfImage;

        public int SizeOfHeaders;

        public int CheckSum;

        public short Subsystem;

        public short DllCharacteristics;

        public int SizeOfStackReserve;

        public int SizeOfStackCommit;

        public int SizeOfHeapReserve;

        public int SizeOfHeapCommit;

        public int LoaderFlags;

        public int NumberOfRvaAndSizes;
        //IMAGE_DATA_DIRECTORY DataDirectory[IMAGE_NUMBEROF_DIRECTORY_ENTRIES];
    }
}
