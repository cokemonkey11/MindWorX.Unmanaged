﻿using System.Runtime.InteropServices;

namespace MindWorX.Unmanaged.Windows
{
    [StructLayout(LayoutKind.Explicit)]
    public unsafe struct ImageSectionHeader
    {
        [FieldOffset(0x00)]
        public fixed byte Name[8];

        #region union Misc;

        [FieldOffset(0x08)]
        public int PhysicalAddress;

        [FieldOffset(0x08)]
        public int VirtualSize;

        #endregion union Misc;

        [FieldOffset(0x0C)]
        public int VirtualAddress;

        [FieldOffset(0x10)]
        public int SizeOfRawData;

        [FieldOffset(0x14)]
        public int PointerToRawData;

        [FieldOffset(0x18)]
        public int PointerToRelocations;

        [FieldOffset(0x1C)]
        public int PointerToLinenumbers;

        [FieldOffset(0x20)]
        public short NumberOfRelocations;

        [FieldOffset(0x22)]
        public short NumberOfLinenumbers;

        [FieldOffset(0x24)]
        public int Characteristics;
    }
}
