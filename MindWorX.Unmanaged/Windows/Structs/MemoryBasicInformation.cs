﻿using System;
using System.Runtime.InteropServices;

namespace MindWorX.Unmanaged.Windows
{
    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct MemoryBasicInformation
    {
        public IntPtr BaseAddress;

        public IntPtr AllocationBase;

        public Page AllocationProtect;

        public int RegionSize;

        public Mem State;

        public Page Protect;

        public Mem Type;
    }
}
