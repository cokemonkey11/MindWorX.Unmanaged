﻿using System;
using System.Runtime.InteropServices;

namespace MindWorX.Unmanaged.Windows
{
    [StructLayout(LayoutKind.Sequential)]
    public struct MenuItemInfo
    {
        public uint cbSize;
        public uint fMask;
        public uint fType;
        public uint fState;
        public uint wID;
        public IntPtr hSubMenu;
        public IntPtr hbmpChecked;
        public IntPtr hbmpUnchecked;
        public IntPtr dwItemData;
        public IntPtr dwTypeData; // string
        public uint cch;
        public IntPtr hbmpItem;
    }
}
